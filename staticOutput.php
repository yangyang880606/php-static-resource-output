<?php
header("content-type:text/html;charset=utf-8");
/**
 *
 *  获取submitForm表单数据
 * 
 */
$title = $_POST["news_title"];
$content = $_POST["news_contents"];

/**
 *
 *  建一个txt,值自增，用作html静态资源文件命名
 * 
 */
$countFile = "countFile.txt";
if (!file_exists($countFile)) { // 文件不存在则创建
  fopen($countFile, "wb");
}
$handle = fopen($countFile, "rb"); // 打开countFile.txt文件，进行写入
$num = fgets($handle, 20); // 获取文件内容
$num = $num + 1; // 文件后缀每次增加1
fclose($handle);

/**
 *
 *  更新$num
 * 
 */
$handle = fopen($countFile, "wb"); // 打开文件，写入
fwrite($handle, $num);
fclose($handle);

/**
 *
 *  获取html路径，可自定义
 * 
 */
$extend = ".html";
$path = "news_" . $num . $extend;

/**
 * 链接本地数据库，插入数据
 */
$link = mysqli_connect("localhost", "root", "");
$sql = "INSERT INTO news(news_title,news_contents,news_path) VALUES('" . $title . "','" . $content . "','" . $path . "');";
mysqli_query($link, $sql);

/**
 *
 *  开始替换模板内容Template.html
 * 
 */
$handle = fopen("template.html", "rb"); //打开html模板
$str = fread($handle, filesize("template.html")); //读取模板内容

$str = str_replace("{news_title}", $title, $str); //替换模板内容
$str = str_replace("{news_contents}", $content, $str);

fclose($handle);

/**
 *
 *  把替换的内容写进生成的html文件
 * 
 */
$handle = fopen($path, "wb");
fwrite($handle, $str);
fclose($handle);

/**
 *
 *  创建完毕后，跳转至新创建的页面
 * 
 */
header("location: $path");
// echo "<script>window.open('$path')</script>";

?>